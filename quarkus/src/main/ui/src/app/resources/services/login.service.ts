import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { iObjects } from 'src/app/views/login/iObjects';
import { API_PATH, K_PATH } from 'src/environments/environment';
import { requestLogin } from '../models/requestLogin';
import { responseLogin } from '../models/responseLogin';


@Injectable({
  providedIn: 'root'
})

export class LoginService {
  constructor(private httpClient: HttpClient) { }



  public doLogin(requestLogin: requestLogin): Observable<responseLogin> {
    //return this.httpClient.post<responseLogin>(`http://localhost:8080/login`, requestLogin);
    return this.httpClient.post<responseLogin>(`${K_PATH}login`, requestLogin);
  }

  public listarTodos(){
    return this.httpClient.get<iObjects[]>(`${API_PATH}objects`).toPromise();
    // return this.httpClient.get<iObjects[]>(`http://localhost:8081/hello/objectsall`).toPromise();
  }

}
