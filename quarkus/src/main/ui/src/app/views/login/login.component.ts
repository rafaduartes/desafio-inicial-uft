import { Component, OnInit } from '@angular/core';
import { requestLogin } from 'src/app/resources/models/requestLogin';
import { LoginService } from 'src/app/resources/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public requestLogin!: requestLogin;

  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
    this.requestLogin = new requestLogin();
  }

  public doLogin(): void {
    console.log(this.requestLogin);
    this.loginService.doLogin(this.requestLogin).subscribe(
      (data) => {
        console.log(data);
      },
      error => {
        console.error(error);
      });
  }

  ListarTodosObjetos(){
    this.loginService.listarTodos()
    .then(objects => console.log(objects))
    .catch(error => console.error(error));
  }
  
}
